#Muskox preprocessing steps
#Each step checked using fastqc

#1.Read quality check using FastQC
fastqc $DNA_HOME/raw_data/160218_E00388_0036_AHLG7YCCXX/Mx8_1_200bp_S1_L001_R1_001.fastq -t 6 -f fastq -o $DNA_HOME/raw_data/160218_E00388_0036_AHLG7YCCXX/  

#2.Lucigen Protocol for Mate pair libraries ONLY

#Split reads/chimeras
python IlluminaChimera-Clean4.py your_R1_inputfilename.fastq

# Junction removal/split
python IlluminaNxSeqJunction-Split7.py mates_ICC4_your_R1_inputfilename.fastq 

#3. Adapter/Primer sequence removal 

#Scythe removes 3' end contamination; It runs PE libraries separately because it doesn't remove reads completely
scythe -a IlluminaAdapterSequences.fasta -o Mx8_1_550bp_S10_L005_R2_001_Scy.fastq -m Mx8_1_550bp_S10_L005_R2_001.txt -n  /media/kyle/Data/muskox/raw_data/160211_D00430_0183_AC908BANXX/Mx8_1_550bp_S10_L005_R2_001.fastq

#EA-Utils Clipper removes adapter/primer sequences from both ends
fastq-mcf -o Mx8_1_550bp_S10_L005_R1_001_scy_eap2.fastq -o Mx8_1_550bp_S10_L005_R2_001_scy_eap2.fastq -t 0.001 -P 25 -f IlluminaAdapterSequences.fasta /media/kyle/Data/muskox/analysis/scy_output/postscythe/Mx8_1_550bp_S10_L005_R1_001_Scy.fastq /media/kyle/Data/muskox/analysis/scy_output/postscythe/Mx8_1_550bp_S10_L005_R2_001_Scy.fastq  

#4. Quality trimming using Sickle
#Had to change the sickle.h file at the phred quality for illumina to min of 0
sickle pe -f /media/kyle/Data/muskox/analysis/eatools/eap/Mx8_1_550bp_S10_L005_R1_001_scy_eap.fastq -r /media/kyle/Data/muskox/analysis/eatools/eap/Mx8_1_550bp_S10_L005_R2_001_scy_eap.fastq -q 20  -l 88 -t illumina -o Mx8_1_550bp_S10_L005_R1_001_scy_eap_sic20.fastq -p Mx8_1_550bp_S10_L005_R2_001_scy_eap_sic20.fastq -s Mx8_1_550bp_S10_L005_singles_001_scy_eap_sic20.fastq

#5. Duplicate Removal using FastUniq
fastuniq -i input_list_5kb -t q -o Mx8_1_5_Kb_S11_L005_R1_001_scy_eap_sic_fu.fastq -p Mx8_1_5_Kb_S11_L005_R2_001_scy_eap_sic_fu.fastq  -c 0

#6. Contaminating Species Read Removal using BBsplit from BBMap
#bins reads based on mapping to multiple reference databases-got databases from downloaded deconseq 
./bbsplit.sh in1=/home/kyle/Muskox/Data/Clean/80x/Mx8_1_550bp_S10_L005_R1_001_scy_eap_sic_fu in2=Mx8_1_550bp_S10_L005_R2_001_scy_eap_sic_fu.fastq ambiguous=best ref=virDB.fasta,bactDB.fasta,hs_ref_GRCh37.fasta refstats=550test.txt basename=out3_550_%.fq outu1=Mx8_1_550bp_S10_L005_R1_001_scy_eap_sic_fu_bb.fastq outu2=Mx8_1_550bp_S10_L005_R2_001_scy_eap_sic_fu_bb.fastq

#depending on fastqc results, repeat steps
