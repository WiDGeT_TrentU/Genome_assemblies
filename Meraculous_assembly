# 1) Reads quality control

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=32000
#SBATCH --time=0-02:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
module load fastqc
fastqc -t 16 -o fastqc_reports *.fq.gz

# 2) Adapter removal and quality trimming
# why use the ktrim=r parameter only (i.e. what about the left side): http://seqanswers.com/forums/showthread.php?t=78397

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=32000
#SBATCH --time=0-12:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
module load bbmap
bbduk.sh usejni=t in1=CP_3408_L1_1.fq.gz in2=CP_3408_L1_2.fq.gz out1=CP_3408_L1_1_clean.fq.gz out2=CP_3408_L1_2_clean.fq.gz ref=/home/USERNAME/project/bbmap/resources/adapters.fa ktrim=r k=23 mink=11 hdist=1 tpe tbo qtrim=rl trimq=20 minlen=50
bbduk.sh usejni=t in1=CP_3408_L2_1.fq.gz in2=CP_3408_L2_2.fq.gz out1=CP_3408_L2_1_clean.fq.gz out2=CP_3408_L2_2_clean.fq.gz ref=/home/USERNAME/project/bbmap/resources/adapters.fa ktrim=r k=23 mink=11 hdist=1 tpe tbo qtrim=rl trimq=20 minlen=50

# 3) Contaminant screening and removal
# Kraken installation (on an interactive node)
git clone https://github.com/DerrickWood/kraken.git
cd kraken

# Setting up the standard Kraken database

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=24
#SBATCH --mem=256000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
module load nixpkgs/16.09 intel/2016.4
module load jellyfish/1.1.11
./kraken-build --standard --threads 24 --db ./StandardDB

# Contaminant screening, a separate job was submitted for each pair of reads

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=24
#SBATCH --mem=256000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
/home/USERNAME/project/kraken/kraken --fastq-input --gzip-compressed --preload -db /home/USERNAME/project/kraken/StandardDB --threads 24 --paired CP_3408_L1_1_clean.fq.gz CP_3408_L1_2_clean.fq.gz --unclassified-out CP_3408_L1_clean_unclass --out-fmt paired --fastq-output > CP_3408_krakenout

# 4) Kmer selection and genome size estimation
# Kmergenie installation (on an interactive node)
module load nixpkgs/16.09 gcc/5.4.0
module load r/3.4.3
wget http://kmergenie.bx.psu.edu/kmergenie-1.7048.tar.gz
tar -xzf kmergenie-1.7048.tar.gz
cd kmergenie-1.7048
make

# A list of reads for input into Kmergenie
cat reads_fileN
# CP_3408_L1_clean_unclass_R1.fastq
# CP_3408_L1_clean_unclass_R2.fastq
# CP_3408_L2_clean_unclass_R1.fastq
# CP_3408_L2_clean_unclass_R2.fastq

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=12
#SBATCH --mem=126000
#SBATCH --time=0-05:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
module load nixpkgs/16.09 gcc/5.4.0
module load r/3.4.3
/home/USERNAME/project/kmergenie-1.7048/kmergenie reads_fileN

# 5) Genome assembly with Meraculous
# See Martchenko et al. 2020 and Meraculous manual for assembly parameter selection process

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=32
#SBATCH --mem=512000
#SBATCH --time=3-00:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=user@trentu.ca
module load nixpkgs/16.09 gcc/4.8.5
module load meraculous/2.2.4
run_meraculous.sh -c meraculous.config